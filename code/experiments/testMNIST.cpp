#include <algorithm>
#include <cassert>
#include <csignal>
#include <fstream>
#include <iterator>
#include <sstream>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <vector>
#include <random>

#include "gmm.h"
#include "sievestreaming.h"
#include "DynamicSieveStreaming.h"
#include "kernel.h"
#include "Experiment.h"

inline void readCSV(std::string const& path,
                    std::vector<std::vector<double>>& X,
                    std::vector<std::string>& tags)
{
    std::ifstream file(path);

    while (!file.eof()) {
        std::string line;

        while (getline(file, line)) {
            std::stringstream ss(line);
            std::string entry;

            std::vector<double> x;
            std::string tag;
            unsigned int i = 0;

            while (getline(ss, entry, ',')) {
                if (i == 0) {
                    tags.push_back(entry.c_str());
                } else {
                    x.push_back(atof(entry.c_str()));
                }

                ++i;
            }

            X.push_back(x);
        }
    }
    file.close();
}

class MyKernel : public Kernel<double> {
private:

public:
    MyKernel(unsigned int dim)
        : Kernel(dim)
    {}

    std::string str() const
    {
        return "no parameters";
    }

    Kernel* clone()
    {
        return static_cast<Kernel*>(new MyKernel(getDim()));
    }

    double val(double const * const x1, double const * const x2) const
    {
        unsigned int cnt = 0;
        for (unsigned int i = 0; i < getDim(); ++i) {
            //if (x1[i] == x2[i]) {
            if (std::abs(x1[i] - x2[i]) < 0.99) {
                cnt++;
            }
        }
        return (float) cnt / getDim();
    }

    double max() const
    {
        return 1;
    }
};

void writeSummary(std::string &filename, std::vector<std::vector<double>> &summary) {
    std::ofstream summaryFile(filename);
    if (!summaryFile.is_open()) {
        std :: cout << "Could not open summary file - Skipping." << std :: endl;
    } else {
        for (auto &entry : summary) {
            bool first = true;
            for (auto x : entry) {
                // TODO: MAYBE DE-UNNORMALIZE DATA?
                if (first) {
                    summaryFile << x;
                    first = false;
                } else {
                    summaryFile << ";" << x;
                }
            }
            summaryFile << std :: endl;
        }
    }
}

int main(int argc, char const* argv[]) {
    std::vector<std::vector<double>> X;
    std::vector<std::string> tags;

    std :: cout << "READING DATA" << std :: endl;
    readCSV("../../data/mnist_train.csv", X, tags);

    std::vector<float> mins;
    std::vector<float> maxs;

    minMaxData(X, mins, maxs);
    normalizeData(X, mins, maxs);

    std::ofstream outFile("mnist_results.csv");
    if (!outFile.is_open()) {
        std :: cout << "Could not open file for saving results!" << std :: endl;
        return 0;
    }

    unsigned int dim = X[0].size();
    float epsilon = 0.1;
    RBFKernel kernel(dim, std::sqrt(784), 1); // 0.8 == 80%

    Experiment exp;
    std::vector<std::vector<double>> summary;
    std::string filename;
    outFile << "NAME;EPSILON;K;" << exp.csvHeader() << std :: endl;

    for (unsigned int K = 8; K < 20; K += 1) {
        std :: cout << "Performing experiments for K = " << K << std :: endl;

        SieveStreaming<double> standard(epsilon, &kernel, K, dim, 1, true);
        exp = runExperiments<SieveStreaming<double>>(standard, X, tags, false, 10, K);
        outFile << "STANDARD;" << epsilon << ";" << K << ";" << exp.toCSV() << std :: endl;
        summary = standard.getBestSummary()->getSummary();
        filename = "summary_standard_" + std::to_string(K) + ".csv";
        writeSummary(filename, summary);

        SieveStreaming<double> less(epsilon, &kernel, K, dim, 1, false);
        exp = runExperiments<SieveStreaming<double>>(less, X, tags, false, 10, K);
        outFile << "LESS;" << epsilon << ";" << K << ";" << exp.toCSV() << std :: endl;
        summary = less.getBestSummary()->getSummary();
        filename = "summary_less_" + std::to_string(K) + ".csv";
        writeSummary(filename, summary);

        DynamicSieveStreaming<double> dynamic(epsilon, &kernel, K, dim, 1);
        exp = runExperiments<DynamicSieveStreaming<double>>(dynamic, X, tags, false, 10, K);
        outFile << "DYNAMIC;" << epsilon << ";" << K << ";" << exp.toCSV() << std :: endl;
        summary = dynamic.getBestSummary()->getSummary();
        filename = "summary_dynamic_" + std::to_string(K) + ".csv";
        writeSummary(filename, summary);
    }
    std :: cout << "DONE" << std :: endl;

    return 0;
}
