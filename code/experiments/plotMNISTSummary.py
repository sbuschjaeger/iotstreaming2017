#!/usr/bin/python3

import numpy as np
import sys
from numpy import genfromtxt
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def dataToMatrix(data):
	tData = None
	for n in data.dtype.names:
		if tData is None:
			tData = data[n]
		else:
			tData = np.column_stack((tData, data[n]))
	return tData

def main(argv):
	print("Loading data")
	data = genfromtxt(argv[0], delimiter=";")
	
	numImages = data.shape[0]
	numRows = int(numImages / 5)
	
	numCols = 5
	if numImages % numCols != 0:
		numRows = numRows + 1

	f, plots = plt.subplots(numRows, numCols)
	row = 0
	col = 0
	for image in data:
		iMatrix = np.reshape(image, (28,28))
		plots[row, col].imshow(iMatrix, cmap="Greys")
		plots[row, col].set_xticklabels([])
		plots[row, col].set_yticklabels([])
		plots[row, col].set_xticks([])
		plots[row, col].set_yticks([])
		col = col + 1
		
		if col >= numCols:
			row = row +1
			col = 0

	if col > 0:
		for i in range(1, numCols - col + 1):
			plots[row, col + i - 1].axis('off')

	f.tight_layout(pad=0.0, w_pad=0.5, h_pad=0.5)
	plt.savefig('mnist_summary.pdf', bbox_inches='tight')
	plt.show()

if __name__ == "__main__":
	main(sys.argv[1:])