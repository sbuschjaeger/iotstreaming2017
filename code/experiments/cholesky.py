#!/usr/bin/python3

import numpy as np
import sys

def testCholesky(n, minimum, sigma):
	X = 2*np.eye(n)

	if (minimum):
		X = X + np.full((n,n), sigma*1)
		X = X - sigma*np.eye(n)

	print(X)
	print("\n")
	print(np.linalg.inv(X))
	print("\n")
	print(np.linalg.cholesky(X))
	print("\n")
	print(0.5*np.log(np.linalg.det(X)))


if __name__ == "__main__":
    testCholesky(int(sys.argv[1]), sys.argv[2], float(sys.argv[3]))