#include <iostream>
#include <chrono>
#include <thread>
#include "hyperboardagent.h"

int main(int argc, char const* argv[]) {
    HyperboardAgent agent;

    //if (connected) {
        std::cout << "Connection established, now sending measurments!" << std :: endl;
        for (unsigned int i = 0; i < 100; ++i) {
            if (!agent.logScalar("TEST", "Test-Group", i, i)) {
                std::cout << "ERROR during connection!" << std::endl;
            }

            std::vector<double> hist;
            for (unsigned int j = 0; j < 50; ++j) {
                hist.push_back(rand());
            }

            if (!agent.logHistogram("TEST", "Test-Group", i, hist)) {
                std::cout << "ERROR during connection!" << std::endl;
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }
//    } else {
//        std::cout << "Could not connect to server" << std::endl;
//    }
//    std :: cout << "Setting up connection" << std ::endl;
//    HyperboardAgent agent;

//    std::cout << "Register parameters" << std :: endl;
//    agent.register_agent("Function-Value", "unitless");

//    std::cout << "DONE" << std :: endl;
}
