#include <algorithm>
#include <cassert>
#include <csignal>
#include <fstream>
#include <iterator>
#include <sstream>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <vector>

#include "Helper.h"
#include "gmm.h"
#include "sievestreaming.h"
#include "DynamicSieveStreaming.h"
#include "kernel.h"
#include "Experiment.h"

inline void readCSV(std::string const& path, std::map<std::string, std::vector<std::vector<double>>>& X,
                    std::map<std::string,std::vector<std::string>>& tags)
{
    std::ifstream file(path);

    while (!file.eof()) {
        std::string line;

        while (getline(file, line)) {
            std::stringstream ss(line);
            std::string entry;

            std::vector<double> x;
            std::string floor;
            std::string building;
            std::string space;
            std::string userid;
            unsigned int i = 0;

            while (getline(ss, entry, ',')) {
//                if (i < 520) {
//                    double rssi = atof(entry.c_str());
//                    if (rssi > 0) {
//                        rssi = -110; // -104 dB denotes ''extremly poor'' signal ==> -105 denots "no" signal
//                    }
//                    x.push_back(rssi);
//                } else {
                switch(i) {
                case 520:
                    x.push_back(atof(entry.c_str()));
                    break;
                case 521:
                    x.push_back(atof(entry.c_str()));
                    break;
                case 522: floor= entry.c_str(); break;
                case 523: building = entry.c_str(); break;
                case 524: space = entry.c_str(); break;
                case 526: userid = entry.c_str(); break;
                default: break;
                }
//                }

                ++i;
            }

            if (X.find(userid) == X.end()) {
                X[userid] = std::vector<std::vector<double>>();
                tags[userid] = std::vector<std::string>();
            }

            X[userid].push_back(x);
            tags[userid].push_back(building + "-" + floor + "-" + space);
        }
    }
    file.close();
}

class TStudent : public Kernel<double> {
private:
    double d;

    double diff(double const * const x1, double const * const x2) const
    {
        double diff = 0.0f;
        for (unsigned i = 0; i < getDim(); ++i) {
            diff += (x1[i] - x2[i]) * (x1[i] - x2[i]);
        }

        return diff;
    }

public:
    TStudent(unsigned int dim)
        : Kernel(dim), d(1.0f)
    {
    }

    TStudent(unsigned int dim, double pD)
        : Kernel(dim), d(pD)
    {
    }

    std::string str() const
    {
        return "d = " + std::to_string(d);
    }

    Kernel* clone()
    {
        return static_cast<Kernel*>(new TStudent(getDim(), d));
    }

    double val(double const * const x1, double const * const x2) const
    {
        return 1.0f / ( 1 + std::pow(diff(x1, x2),d) );
    }

    double max() const
    {
        return 1;
    }
};

class MyKernel : public Kernel<double> {
private:

public:
    MyKernel(unsigned int dim)
        : Kernel(dim)
    {}

    std::string str() const
    {
        return "no parameters";
    }

    Kernel* clone()
    {
        return static_cast<Kernel*>(new MyKernel(getDim()));
    }

    double val(double const * const x1, double const * const x2) const
    {
        unsigned int cnt = 0;
        for (unsigned int i = 0; i < getDim(); ++i) {
            //if (x1[i] == x2[i]) {
            if (std::abs(x1[i] -x2[i]) < 5) {
                cnt++;
            }
        }
        return (float) cnt / getDim();
    }

    double max() const
    {
        return 1;
    }
};

int main(int argc, char const* argv[]) {
    std::map<std::string, std::vector<std::vector<double>>> measurments;
    std::map<std::string, std::vector<std::string>> tags;
    readCSV("../../data/UJIndoorLoc/trainingData.csv", measurments, tags);

    unsigned int totalNumTags = 0;
    for (auto &e : measurments) {
        std::vector<std::vector<double>> userX = measurments[e.first];

        std::vector<float> mins;
        std::vector<float> maxs;

        minMaxData(userX, mins, maxs);
        normalizeData(userX, mins, maxs);
        measurments[e.first] = userX;

        std::vector<std::string> tTags(tags[e.first]);
        std::sort(tTags.begin(), tTags.end());
        totalNumTags += std::unique(tTags.begin(), tTags.end()) - tTags.begin();
    }
    std :: cout << "No user: " << measurments.size() << std :: endl;
    std :: cout << "Avg no tags per user: " << (float) totalNumTags / measurments.size() << std :: endl;

    std::ofstream outFile("ujindoor_results.csv");
    if (!outFile.is_open()) {
        std :: cout << "Could not open file for saving results!" << std :: endl;
        return 0;
    }

    Experiment exp;
    unsigned int dim = measurments["2"][0].size();
    float epsilon = 0.1;
    RBFKernel kernel(dim, 0.005, 1); //0.01
    outFile << "NAME;EPSILON;K;" << exp.csvHeader() << std :: endl;

    for (unsigned int K = 75; K < 200; K += 5) {
        std :: cout << "Performing experiments for K = " << K << std :: endl;
        std::vector<Experiment> standardSieve;
        std::vector<Experiment> lessSieves;
        std::vector<Experiment> dynamicSieves;

        for (auto &e: measurments) {
            std::vector<std::vector<double>> userX = measurments[e.first];
            std::vector<std::string> userTags = tags[e.first];

            SieveStreaming<double> standard(epsilon, &kernel, K, dim, 1, true);
            exp = runExperiments<SieveStreaming<double>>(standard, userX, userTags, false, 10, K);
            standardSieve.push_back(exp);

            SieveStreaming<double> less(epsilon, &kernel, K, dim, 1, false);
            exp = runExperiments<SieveStreaming<double>>(less, userX, userTags, false, 10, K);
            lessSieves.push_back(exp);

            DynamicSieveStreaming<double> dynamic(epsilon, &kernel, K, dim, 1);
            exp = runExperiments<DynamicSieveStreaming<double>>(dynamic, userX, userTags,false, 10, K);
            dynamicSieves.push_back(exp);
        }
        exp = avg(standardSieve);
        outFile << "STANDARD;" << epsilon << ";" << "K;" << exp.toCSV() << std :: endl;

        exp = avg(lessSieves);
        outFile << "LESS;" << epsilon << ";" << "K;" << exp.toCSV() << std :: endl;

        exp = avg(dynamicSieves);
        outFile << "DYNAMIC;" << epsilon << ";" << "K;" << exp.toCSV() << std :: endl;
    }

    return 0;
}
