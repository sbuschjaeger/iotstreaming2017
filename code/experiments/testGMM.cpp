#include <algorithm>
#include <cassert>
#include <csignal>
#include <fstream>
#include <iterator>
#include <sstream>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <vector>

#include "gmm.h"
#include "sievestreaming.h"
#include "DynamicSieveStreaming.h"
#include "kernel.h"
#include "Experiment.h"

int main(int argc, char const* argv[]) {
    Gaussian g1({0.0,1,-2,0.3}, {0.5,0.5,0.5,0.5});
    Gaussian g2({-2.5,0.5,1,3.5}, {0.1,0.5,0.1,0.5});
    Gaussian g3({0.5,2,0.5,2}, {0.8,0.5,0.8,0.5});
    Gaussian g4({0.7,0.7,0.7,0.7}, {0.1,0.8,0.8,0.1});
    Gaussian g5({1,3,1,3}, {0.5,0.1,0.5,0.1});
    Gaussian g6({3,1,3,1}, {0.5,0.8,0.5,0.8});
    Gaussian g7({-2,2,-2,2}, {0.2,0.5,0.2,0.5});
    Gaussian g8({-2,2,-2,2}, {0.5,0.2,0.5,0.2});

    std :: cout << "=== Test for equal distribution === " << std :: endl;
    std :: cout << "GENERATING DATA" << std :: endl;

    std::vector<std::vector<double>> X;
    std::vector<std::string> tags;
    MixtureModel mEqual({0.001, 0.001, 0.133, 0.133, 0.133, 0.133, 0.133, 0.133},
                   {&g1,&g2,&g3,&g4,&g5,&g6,&g7,&g8});

    for(unsigned int i = 0; i < 5000; ++i) {
        std::pair<std::string, std::vector<double>> x = mEqual.generate(4);
        tags.push_back(x.first);
        X.push_back(x.second);
    }

    std::ofstream outFile("gmm_results.csv");
    if (!outFile.is_open()) {
        std :: cout << "Could not open file for saving results!" << std :: endl;
        return 0;
    }

    float epsilon = 0.1; // 0.1
    unsigned int dim = X[0].size();
    RBFKernel rbf(dim, 5*std::sqrt(dim), 1); // 2*
    Experiment exp;
    outFile << "NAME;EPSILON;K;" << exp.csvHeader() << std :: endl;

    for (unsigned int K = 10; K < 50; K += 2) {
        std :: cout << "Performing experiments for K = " << K << std :: endl;

        SieveStreaming<double> standard(epsilon, &rbf, K, dim, 1, true);
        exp = runExperiments<SieveStreaming<double>>(standard, X, tags, true, 10, K);
        outFile << "STANDARD;" << epsilon << ";" << K << ";" << exp.toCSV() << std :: endl;

        SieveStreaming<double> less(epsilon, &rbf, K, dim, 1, false);
        exp = runExperiments<SieveStreaming<double>>(less, X, tags, true, 10, K);
        outFile << "LESS;" << epsilon << ";" << K << ";" << exp.toCSV() << std :: endl;

        DynamicSieveStreaming<double> dynamic(epsilon, &rbf, K, dim, 1);
        exp = runExperiments<DynamicSieveStreaming<double>>(dynamic, X, tags, true, 10, K);
        outFile << "DYNAMIC;" << epsilon << ";" << K << ";" << exp.toCSV() << std :: endl;
    }

//    std :: cout << "=== Test for un-equal distribution === " << std :: endl;
//    std::cout << "START ON ORIGINAL INTERVAL" << std::endl;
//    SieveStreaming<double> sStream = new sStream(0.1, rbf, 20, 4, 1, true);

//    MixtureModel unEqual({0.4, 0.2, 0.1, 0.1, 0.075, 0.075, 0.025, 0.025},
//                   {&g1,&g2,&g3,&g4,&g5,&g6,&g7,&g8});
//    performTest(unEqual, sStream);
    return 0;
}
