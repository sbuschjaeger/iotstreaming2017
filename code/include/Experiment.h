#ifndef EXPERIMENT_H
#define EXPERIMENT_H

struct Experiment {
    float runtime;
    float fValue;
    float memUsed;
    float trueHistCnt;
    float extractedHistCnt;
    float elem;

    Experiment() {
        trueHistCnt = 0;
        extractedHistCnt = 0;
        runtime = 0;
        fValue = 0;
        elem = 0;
        memUsed = 0;
    }

    std::string toCSV() const {
        std::string s;
        s += std::to_string(runtime) + ";";
        s += std::to_string(runtime/(float) elem) + ";";
        s += std::to_string(fValue) + ";";
        s += std::to_string(trueHistCnt) + ";";
        s += std::to_string(extractedHistCnt) + ";";
        s += std::to_string((float)extractedHistCnt / (float)trueHistCnt) + ";";
        s += std::to_string(elem) + ";";
        s += std::to_string(memUsed);

        return s;
    }

    std::string csvHeader() const {
        std::string s;
        s += "total_runtime;";
        s += "runtime_per_elem;";
        s += "fValue;";
        s += "trueHistCnt;";
        s += "extractedHistCnt;";
        s += "histPercentage;";
        s += "elem;";
        s += "memory";

        return s;
    }

    std::string str() const {
        std::string s;
        s += "total runtime = " + std::to_string(runtime) + "\n";
        s += "runtime per elem = " + std::to_string(runtime/(float) elem) + "\n";
        s += "fValue = " + std::to_string(fValue) + "\n";
        s += "trueHistCnt = " + std::to_string(trueHistCnt) + "\n";
        s += "extractedHistCnt = " + std::to_string(extractedHistCnt) + "\n";
        s += "histPercentage = " + std::to_string((float)extractedHistCnt / (float)trueHistCnt) + "\n";
        s += "elem = " + std::to_string(elem) + "\n";
        s += "memory = " + std::to_string(memUsed) + "\n";
        return s;
    }
};

Experiment avg(std::vector<Experiment> &experiments) {
    Experiment exp;

    for (auto e : experiments) {
        exp.runtime += e.runtime;
        exp.fValue += e.fValue;
        exp.elem += e.elem;
        exp.memUsed += e.memUsed;
        exp.extractedHistCnt += e.extractedHistCnt;
        exp.trueHistCnt += e.trueHistCnt;
    }

    exp.runtime /= experiments.size();
    exp.fValue /= experiments.size();
    exp.elem /= experiments.size();
    exp.memUsed /= experiments.size();
    exp.extractedHistCnt /= experiments.size();
    exp.trueHistCnt /= experiments.size();

    return exp;
}

template <typename SieveType>
Experiment runExperiment(SieveType &sStream, std::vector<std::vector<double>> &X, std::vector<std::string> &tags,
                       bool verbose) {
    Experiment exp;

    if (verbose) {
        std :: cout << "X.size() = " << X.size() << std :: endl;
    }

    std::map<std::string, unsigned int> firstOccurence;
    std::map<std::string, unsigned int> firstAdded;

    auto start = std::chrono::high_resolution_clock::now();
    for (unsigned int i = 0; i < X.size(); ++i) {
        exp.elem++;
        if (verbose && i % 10000 == 0) {
            std :: cout << (float) i / X.size() * 100.0f << " % done" << std :: endl;
        }

        bool added = sStream.next(&X[i][0], tags[i]);
        if (firstOccurence.find(tags[i]) == firstOccurence.end()) {
            firstOccurence[tags[i]] = i;
        }

        if (added && firstAdded.find(tags[i]) == firstAdded.end()) {
            firstAdded[tags[i]] = i;
        }

        if (sStream.allSievesDone()) {
            break;
        }
    }

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start);
    exp.runtime = duration.count();
    exp.memUsed = getPhysicalMemoryUsed();

    std::map<std::string, unsigned int> trueHistogram;

    for (auto tag : tags) {
        if (trueHistogram.find(tag) == trueHistogram.end()) {
            trueHistogram[tag] = 1;
        } else {
            trueHistogram[tag]++;
        }
    }

    TaggedSieve<double> const * sieve = sStream.getBestSummary();
    exp.fValue = sieve->getCurFunctionValue();

    std::map<std::string, unsigned int> histogram;

    for (auto s :  sieve->getTags()) {
        if (histogram.find(s) == histogram.end()) {
            histogram[s] = 1;
        } else {
            histogram[s]++;
        }
    }
    exp.trueHistCnt = trueHistogram.size();
    exp.extractedHistCnt = histogram.size();

    if (verbose) {
        std::cout << "Looked at " << exp.elem << " elements" << std ::endl;

        std ::cout << "True-Histogram of tags: " << trueHistogram.size() << std :: endl;
        for (auto &x : trueHistogram) {
            std :: cout << x.first << ": \t" << x.second << std :: endl;
        }
        std::cout << std::endl;
        std ::cout << "Extracted-Histogram of tags: " << histogram.size() << std :: endl;
        for (auto &x : histogram) {
            std :: cout << x.first << ": \t" << x.second << std :: endl;
        }
        std::cout << std::endl;
        std ::cout << "First occurances of tags in stream: " << firstOccurence.size() << std :: endl;
        for (auto &x : firstOccurence) {
            std :: cout << x.first << ": \t" << x.second << std :: endl;
        }
        std::cout << std::endl;
        std ::cout << "First time added to summary:" << firstAdded.size() << std :: endl;
        for (auto &x : firstAdded) {
            std :: cout << x.first << ": \t" << x.second << std :: endl;
        }
    }

    return exp;
}

// NOTE: Since we suffle X and tags in this function, we provide a COPY of it!
template <typename SieveType>
Experiment runExperiments(SieveType &sStream, std::vector<std::vector<double>> X,
                         std::vector<std::string> tags, bool verbose, unsigned int numRuns, unsigned int seed) {
    std::vector<Experiment> experiments;
    Experiment results;

    for (unsigned int i = 0; i < numRuns; ++i) {
        std::srand ( seed );
        std::random_shuffle(std::begin(X), std::end(X));
        std::srand ( seed );
        std::random_shuffle(std::begin(tags), std::end(tags));

        sStream.clear();
        Experiment exp = runExperiment<SieveType>(sStream, X, tags, verbose);
        experiments.push_back(exp);
    }

    return avg(experiments);
}

#endif
