#ifndef GPEXPERTWORKER_H
#define GPEXPERTWORKER_H

/*#include <future>
#include <queue>

template <typename feature_t, typename target_t>
class GPExpert;

template <typename feature_t, typename target_t>
class GPExpertWorker {
private:
    friend class GPExpert<feature_t, target_t>;

    std::mutex mutex;
    std::queue<GPExpert<feature_t, target_t>*> jobs;

    unsigned int curNoThreads;
    unsigned int remainingWorkload;

    const unsigned int NUM_THREADS;

    bool accepting;

    void startThreadNextIfPossible()
    {
        if (!jobs.empty()) {
            GPExpert<feature_t, target_t>* job = jobs.front();
            jobs.pop();
            std::thread t([this](GPExpert<feature_t, target_t>* gp) {
                unsigned int workload = gp->indices->size();
                gp->trainSubset();
                this->threadDone(workload);
            },
                job);
            t.detach();
            curNoThreads++;
        }
    }

public:
    GPExpertWorker(unsigned int NUM_THREADS)
        : NUM_THREADS(NUM_THREADS)
        , accepting(true)
    {
        curNoThreads = 0;
        remainingWorkload = 0;
    }

    unsigned int getRemainingWorkload() const
    {
        return remainingWorkload;
    }

    unsigned int curNumThreads() const
    {
        return curNoThreads;
    }

    unsigned int remainingJobs()
    {
        mutex.lock();
        unsigned int num = jobs.size();
        mutex.unlock();
        return num;
    }

    unsigned int maxNumThreads() const
    {
        return NUM_THREADS;
    }

    void threadDone(unsigned int workload_processed)
    {
        mutex.lock();
        curNoThreads--;
        this->remainingWorkload -= workload_processed;
        startThreadNextIfPossible();
        mutex.unlock();
    }

    void stop()
    {
        mutex.lock();
        accepting = false;

        std::queue<GPExpert<feature_t, target_t>*> empty;
        std::swap(jobs, empty);

        // block
        mutex.unlock();
    }

    void reset()
    {
        mutex.lock();
        accepting = true;
        mutex.unlock();
    }

    void addJob(GPExpert<feature_t, target_t>* gp)
    {
        mutex.lock();
        if (accepting) {
            jobs.push(gp);
            remainingWorkload += gp->indices->size();
            if (curNoThreads < NUM_THREADS) {
                startThreadNextIfPossible();
            }
        }
        mutex.unlock();
    }
};
*/
#endif // GPEXPERTWORKER_H
