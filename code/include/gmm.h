#ifndef GMM_H
#define GMM_H

#include <random>

class Distribution {
public:
    virtual std::vector<double> generate(unsigned dim) = 0;
    virtual ~Distribution() {}
};

class MixtureModel {
private:
    std::vector<double> probabilities;
    std::vector<Distribution *> distributions;
    std::discrete_distribution<> d;
    std::default_random_engine gen;

public:

    MixtureModel(std::vector<double> const &pProbabilities, std::vector<Distribution*> const &pDistributions)
        : probabilities(pProbabilities), distributions(pDistributions), d(pProbabilities.begin(), pProbabilities.end()) {
    }

    ~MixtureModel() {
        distributions.clear();
    }

    std::pair<std::string, std::vector<double>> generate(unsigned int dim) {
        unsigned int dist = d(gen);
        std::vector<double> example = distributions[dist]->generate(dim);
        std::string name = "Distribution-" + std::to_string(dist);
        return std::make_pair(name, example);
    }

};

class Gaussian : public Distribution {
private:
    std::vector<double> mus;
    std::vector<double> sigmas;
    std::vector<std::normal_distribution<double>> gaussians;
    std::default_random_engine gen;

public:
    Gaussian(std::vector<double> const &pMus, std::vector<double>const &pSigmas) : mus(pMus), sigmas(pSigmas) {
        for (unsigned int i = 0; i < mus.size(); ++i) {
            gaussians.push_back(std::normal_distribution<double>(mus[i],sigmas[i]));
        }

    }

    ~Gaussian() {
        gaussians.clear();
    }

    std::vector<double> generate(unsigned int dim) {
        std::vector<double> vec;
        for(unsigned int i = 0; i < dim; ++i) {
            vec.push_back(gaussians[i](gen));
        }

        return vec;
    }

};


#endif // GMM_H
