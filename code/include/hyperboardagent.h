#ifndef HYPERBOARDAGENT_H
#define HYPERBOARDAGENT_H

#include "curlpp/cURLpp.hpp"
#include "curlpp/Options.hpp"
#include "curlpp/Easy.hpp"

#include <vector>
#include <sstream>
#include <fstream>

class HyperboardAgent {
private:
    std::string address;

    // TODO: CHANGE THIS
    std::string urlencode(std::string const &url) const {
        std::string s = "";
        for (char c : url) {
            if (c == '+') {
                s += "%2B";
            } else {
                s += c;
            }
        }

        return s;
    }

    bool send(std::string const &url) const {
        try {
            std::list<std::string> header;
            header.push_back("Content-Type: application/x-www-form-urlencoded");

            curlpp::Easy r;
            //r.setOpt(new curlpp::options::Verbose(true));
            r.setOpt(new curlpp::options::Verbose(false));
            r.setOpt(new curlpp::options::Url(url));
            r.setOpt(new curlpp::options::HttpHeader(header));

            std::ostringstream response;
            r.setOpt(new curlpp::options::WriteStream(&response));
            r.perform();

            return !response.str().compare("good");
        } catch(...) {
            return false;
        }
    }

public:
    HyperboardAgent() : address("127.0.0.1:5000") {}

    HyperboardAgent(const std::string &pAddress) :
        address(pAddress) {}

    bool reset() const {
        std::string url = address + "/reset";
        return send(url);
    }

    bool logScalar(const std::string &pName, const std::string &pGroup, unsigned int pIndex, double pValue) const {
        std::string url = address + "/scalar?";
        url += "name=" + pName + "&";
        url += "value=" + std::to_string(pValue) + "&";
        url += "group=" + pGroup + "&";
        url += "time=" + std::to_string(pIndex);

        return send(urlencode(url));
    }

    bool logHistogram(const std::string &pName, const std::string &pGroup, unsigned int pIndex, std::vector<unsigned int> const &pValues) const {
        std::vector<double> fVec(pValues.begin(), pValues.end());

        return logHistogram(pName, pGroup, pIndex, fVec);
    }

    bool logHistogram(const std::string &pName, const std::string &pGroup, unsigned int pIndex, std::vector<double> const &pValues) const {
        std::string url = address + "/histogram?";
        url += "name=" + pName + "&";
        url += "time=" + std::to_string(pIndex) + "&";
        url += "group=" + pGroup;

        std::stringstream ss;
        for (float val : pValues) {
            ss << "&value=" << val;
        }

        return send(urlencode(url + ss.str()));
    }
};

#endif // HYPERBOARDAGENT_H
