#ifndef DYNAMIC_SIEVE_STREAMING_H
#define DYNAMIC_SIEVE_STREAMING_H

#include "sievestreaming.h"

template <typename flt_t>
class DynamicSieveStreaming : public SieveStreaming<flt_t> {
private:

public:

    DynamicSieveStreaming(flt_t pEpsilon, Kernel<flt_t>* pKernel,
                          unsigned int pSummarySize, unsigned int pDim,
                          flt_t pSigmaNoise)
        : SieveStreaming<flt_t>(pEpsilon,pKernel,pSummarySize, pDim, pSigmaNoise, false)
    { }

    void clear() {
        SieveStreaming<flt_t>::clear();
    }

    bool next(flt_t const * const elem, std::string const &tag) {
        bool addedOnce = false;
        ++this->cnt;

        std::vector<unsigned int> Ns;
        std::vector<flt_t> Fs;

        unsigned int numReopenSieves = 0;
        flt_t smallestOpenThreshold = std::numeric_limits<flt_t>::max();

        auto sieveIt = this->activeSieves.begin();
        while(sieveIt != this->activeSieves.end()) {
            TaggedSieve<flt_t> *sieve = (*sieveIt);

            if (sieve->next(elem, tag)) {
                addedOnce = true;
            }
            Ns.push_back(sieve->getN());
            Fs.push_back(sieve->getCurFunctionValue());

            if (sieve->isClosed()) {
                this->closedSieves.push_back(sieve);
                sieveIt = this->activeSieves.erase(sieveIt);
                ++numReopenSieves;
//                std::cout << "closing sieve with f-value: " << sieve->getCurFunctionValue()
//                          << " and threshold: " << sieve->getThreshold() << std :: endl;
            } else {
                if (sieve->getThreshold() < smallestOpenThreshold) {
                    smallestOpenThreshold = sieve->getThreshold();
                }
                ++sieveIt;
            }
        }

        flt_t largestClosedThreshold = 0;
        for (auto sieve : this->closedSieves) {
            Ns.push_back(sieve->getN());
            Fs.push_back(sieve->getCurFunctionValue());

            if (sieve->getThreshold() > largestClosedThreshold) {
                largestClosedThreshold = sieve->getThreshold();
            }
        }

//        std :: cout << "numReopenSieves = " << numReopenSieves << std :: endl;
//        std :: cout << "largestClosedThreshold = " << largestClosedThreshold << std :: endl;
//        std :: cout << "smallestOpenThreshold = " << smallestOpenThreshold << std :: endl;

        if (numReopenSieves > 0) {
            if (largestClosedThreshold > smallestOpenThreshold) {
                // A sieve with higher threshold was closed early than other sieves
                // This is due to dynamic inserting sieves ==> Need to close all other sieves
                smallestOpenThreshold = std::numeric_limits<flt_t>::max();

                auto sieveIt = this->activeSieves.begin();
                while(sieveIt != this->activeSieves.end()) {
                    TaggedSieve<flt_t> *sieve = (*sieveIt);

                    if (sieve->getThreshold() > largestClosedThreshold && sieve->getThreshold() < smallestOpenThreshold) {
                        smallestOpenThreshold = sieve->getThreshold();
                    }

                    if (sieve->getThreshold() <= largestClosedThreshold) {
                        this->activeSieves.erase(sieveIt);
                        ++numReopenSieves;
                        //std::cout << "deleting sieve with f-value: " << sieve->getCurFunctionValue() << std :: endl;
                    } else {
                        ++sieveIt;
                    }
                }
            }

            if (smallestOpenThreshold == std::numeric_limits<flt_t>::max()) {
                flt_t m = std::log(1+SieveStreaming<flt_t>::sigmaNoise*SieveStreaming<flt_t>::sigmaNoise*SieveStreaming<flt_t>::kernel->max());
                flt_t step = (2*SieveStreaming<flt_t>::summarySize*m - largestClosedThreshold) / numReopenSieves;
                //std :: cout << "2*k*m = " << 2*summarySize*m << std ::endl;
                for (unsigned int i = 1; i <= numReopenSieves; ++i) {
                    flt_t threshold = largestClosedThreshold + step*i;
                    //std :: cout << "\t new threshold " << threshold << std :: endl;
                    TaggedSieve<flt_t> *sieve = new TaggedSieve<flt_t>(SieveStreaming<flt_t>::kernel, SieveStreaming<flt_t>::summarySize,SieveStreaming<flt_t>::dim,SieveStreaming<flt_t>::sigmaNoise,threshold);
                    this->activeSieves.push_back(sieve);

                }

            } else if(largestClosedThreshold > 0){
                flt_t stepsize = (smallestOpenThreshold - largestClosedThreshold) / (numReopenSieves + 1);

                for (unsigned int i = 1; i <= numReopenSieves; ++i) {
                    flt_t threshold = largestClosedThreshold + i*stepsize;
                    TaggedSieve<flt_t> *sieve = new TaggedSieve<flt_t>(SieveStreaming<flt_t>::kernel, SieveStreaming<flt_t>::summarySize,SieveStreaming<flt_t>::dim,SieveStreaming<flt_t>::sigmaNoise,threshold);
                    this->activeSieves.push_back(sieve);
                }
//                std :: cout << "Added " << numReopenSieves << " sieves with threhsolds in ["
//                            << largestClosedThreshold << "," << smallestOpenThreshold << "]" << std :: endl;
            }
        }

        //std::cout << "open = " << open << std::endl;
        this->logStatistics(Ns, Fs);
        return addedOnce;
    }

};


#endif
