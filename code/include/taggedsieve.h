#ifndef TAGGEDSIEVE_H
#define TAGGEDSIEVE_H

#include <string>

#include "Sieve.h"

template <typename flt_t>
class TaggedSieve : public Sieve<flt_t> {
private:
    std::vector<std::string> tags;

public:

    TaggedSieve(Kernel<flt_t>* pKernel, unsigned int pSummarySize, unsigned int pDim, flt_t pSigmaNoise, flt_t pThreshold)
        : Sieve<flt_t>(pKernel, pSummarySize, pDim, pSigmaNoise, pThreshold) { }

    bool next(flt_t const * const elem, std::string const &tag) {
        if (Sieve<flt_t>::next(elem)) {
            tags.push_back(tag);
            return true;
        } else {
            return false;
        }
    }

    void clear() {
        tags.clear();
        Sieve<flt_t>::clear();
    }

    std::vector<std::string> getTags() const {
        return tags;
    }
};

#endif // TAGGEDSIEVE_H
