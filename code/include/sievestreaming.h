#ifndef SIEVESTREAMING_H
#define SIEVESTREAMING_H

#include "taggedsieve.h"
#include "hyperboardagent.h"
#include "StatHelper.h"

template <typename flt_t>
class SieveStreaming {
protected:
    std::vector<TaggedSieve<flt_t>*> closedSieves;
    std::vector<TaggedSieve<flt_t>*> activeSieves;
    unsigned int numTotalSieves;

    unsigned int cnt;

    HyperboardAgent logger;

    std::pair<TaggedSieve<flt_t>* const, flt_t> getBestSummary(std::vector<TaggedSieve<flt_t>*> const & summaries) const {
        TaggedSieve<flt_t>* bestSummary = NULL;
        flt_t maxF = 0;

        for (auto sieve : summaries)  {
            if (bestSummary == NULL) {
                bestSummary = sieve;
                maxF = sieve->getCurFunctionValue();
            } else {
                if (sieve->getCurFunctionValue() > maxF) {
                    maxF = sieve->getCurFunctionValue();
                    bestSummary = sieve;
                }
            }
        }

        return std::make_pair(bestSummary, maxF);
    }

    flt_t epsilon;
    Kernel<flt_t>* kernel;
    unsigned int summarySize;
    unsigned int dim;
    flt_t sigmaNoise;
    bool originalInterval;

public:
    SieveStreaming(flt_t pEpsilon, Kernel<flt_t>* pKernel, unsigned int pSummarySize,
                   unsigned int pDim, flt_t pSigmaNoise, bool pOriginalInterval) {
        epsilon = pEpsilon;
        kernel = pKernel;
        summarySize = pSummarySize;
        dim = pDim;
        sigmaNoise = pSigmaNoise;
        originalInterval = pOriginalInterval;
    }

    ~SieveStreaming() {
        for (auto s : activeSieves) {
            delete s;
        }
        activeSieves.clear();

        for (auto s : closedSieves) {
            delete s;
        }
        closedSieves.clear();
    }

    void init() {
        flt_t m = std::log(1+sigmaNoise*sigmaNoise*kernel->max());

        int lower, upper;
        flt_t tlower;
        if (originalInterval) {
            tlower = std::log(m)/std::log(1+epsilon);
        } else {
            tlower = std::log(std::log(1+summarySize*sigmaNoise*sigmaNoise*kernel->max()))/std::log(1+epsilon);
        }

        if (tlower > 0) {
            lower = (int)tlower + 1;
        } else {
            lower = (int)tlower;
        }

        flt_t tupper = std::log(summarySize*m)/std::log(1+epsilon);
        upper = (int) tupper; // "+ 1" for testing

//        std :: cout << "lower = " << lower << std :: endl;
//        std :: cout << "upper = " << upper << std :: endl;

        for (int i = lower; i < upper; ++i) {
            flt_t threshold = std::pow(1+epsilon, i);
            //std :: cout << "New threshold: " << threshold << std :: endl;
            TaggedSieve<flt_t> *sieve = new TaggedSieve<flt_t>(kernel, summarySize, dim, sigmaNoise, threshold);
            activeSieves.push_back(sieve);
        }

        numTotalSieves = activeSieves.size();
        cnt = 0;
        //std :: cout << "Number of sieves: " << numTotalSieves << std :: endl;
    }

    void clear() {
        for (auto s : activeSieves) {
            delete s;
        }
        activeSieves.clear();

        for (auto s : closedSieves) {
            delete s;
        }
        closedSieves.clear();

        init();
    }

    TaggedSieve<flt_t> const * const getBestSummary() const {
        std::pair<TaggedSieve<flt_t>* const, flt_t> p1 = getBestSummary(activeSieves);
        std::pair<TaggedSieve<flt_t>* const, flt_t> p2 = getBestSummary(closedSieves);

        if (p1.first == NULL) {
            return p2.first;
        } else if (p2.first == NULL) {
            return p1.first;
        } else {
            if (p1.second > p2.second) {
                return p1.first;
            } else {
                return p2.first;
            }
        }
    }

    void logStatistics(std::vector<unsigned int > const &Ns, std::vector<flt_t> const &Fs) {
        flt_t var,mean;
        std::pair<unsigned int, unsigned int> minMaxN = statistics::minMaxData<unsigned int>(Ns);
        var = statistics::variance<unsigned int>(Ns);
        mean = statistics::mean<unsigned int>(Ns);

        logger.logScalar("minN", "N", cnt, minMaxN.first);
        logger.logScalar("maxN", "N", cnt, minMaxN.second);
        logger.logScalar("avgN", "N", cnt, mean);
        logger.logScalar("varN", "sigma", cnt, var);

        std::pair<flt_t, flt_t> minMaxF = statistics::minMaxData<flt_t>(Fs);
        var = statistics::variance<flt_t>(Fs);
        mean = statistics::mean<flt_t>(Fs);
        //std::cout << "mean = " << mean << std::endl;
        logger.logScalar("minF", "logdet", cnt, minMaxF.first);
        logger.logScalar("maxF", "logdet", cnt, minMaxF.second);
        logger.logScalar("avgF", "logdet", cnt, mean);
        logger.logScalar("varF", "sigma", cnt, var);

        logger.logHistogram("fVal", "fVal", cnt, Fs);
        logger.logHistogram("N", "N", cnt, Ns);
    }

    bool allSievesDone() const {
        return activeSieves.size() == 0;
    }

    bool next(flt_t const * const elem, std::string const &tag) {
        ++cnt;
        bool addedOnce = false;

        std::vector<unsigned int> Ns;
        std::vector<flt_t> Fs;

        auto sieveIt = activeSieves.begin();
        while(sieveIt != activeSieves.end()) {
            TaggedSieve<flt_t> *sieve = (*sieveIt);

            if (sieve->next(elem, tag)) {
                addedOnce = true;
            }
            Ns.push_back(sieve->getN());
            Fs.push_back(sieve->getCurFunctionValue());

            if (sieve->isClosed()) {
                closedSieves.push_back(sieve);
                sieveIt = activeSieves.erase(sieveIt);
            } else {
                ++sieveIt;
            }
        }

        for (auto sieve : closedSieves){
            Ns.push_back(sieve->getN());
            Fs.push_back(sieve->getCurFunctionValue());
        }

        //std::cout << "open = " << open << std::endl;
        logStatistics(Ns, Fs);
        return addedOnce;
    }
};

#endif // SIEVESTREAMING_H
