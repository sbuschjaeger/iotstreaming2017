#ifndef STAT_HELPER
#define STAT_HELPER

#include <utility>

namespace statistics {

template <typename T>
inline std::pair<float, float> minMaxData(std::vector<T> const& X)
{
    float min,max;
    bool first = true;

    for (auto x : X) {
        if (first) {
            min = x;
            max = x;
        } else {
            if (x < min) {
                min = x;
            }
            if (x > max) {
                max = x;
            }
        }
        first = false;
    }

    return std::make_pair(min,max);
}

template <typename T>
inline float mean(std::vector<T> const& X)
{
    float sum = 0.0;
    for (auto x : X) {
        sum += x;
    }
    return sum / X.size();
}

template <typename T>
inline float variance(std::vector<T> const& X)
{
    float m = mean(X);

    float var = 0.0;
    for (auto x : X) {
        var += (x - m) * (x - m);
    }

    return var / (X.size() - 1);
}

}

#endif
