#ifndef KERNEL_H
#define KERNEL_H

#include "Helper.h"
#include <string>

template <typename flt_t>
class Kernel {
private:
    unsigned int dim;

public:
    Kernel(unsigned int pDim) : dim(pDim) {}

    virtual ~Kernel() {}

    unsigned int getDim() const {
        return dim;
    }

    virtual flt_t val(flt_t const * const x1, flt_t const * const x2) const = 0;
    virtual flt_t max() const = 0;

    virtual std::string str() const = 0;
    virtual Kernel* clone() = 0;
};

class RBFKernel : public Kernel<double> {
private:
    double ell;
    double sigma;

    double diff(double const * const x1, double const * const x2) const
    {
        double diff = 0.0f;
        for (unsigned i = 0; i < getDim(); ++i) {
            diff += (x1[i] - x2[i]) * (x1[i] - x2[i]);
        }

        return diff;
    }

public:
    RBFKernel(unsigned int dim)
        : Kernel(dim), ell(1.0f)
        , sigma(1.0f)
    {
    }

    RBFKernel(unsigned int dim, double pEll, double pSigma)
        : Kernel(dim), ell(pEll)
        , sigma(pSigma)
    {
    }

    std::string str() const
    {
        return "sigma0 = " + std::to_string(sigma) + " - ell1 = " + std::to_string(ell);
    }

    Kernel* clone()
    {
        return static_cast<Kernel*>(new RBFKernel(getDim(), ell, sigma));
    }

    double val(double const * const x1, double const * const x2) const
    {
        return sigma * sigma * std::exp(-1.0f * diff(x1, x2) / (ell * ell));
    }

    double max() const
    {
        return sigma * sigma;
    }
};

#endif // KERNEL_H
