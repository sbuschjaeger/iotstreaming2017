#ifndef HELPER_H
#define HELPER_H

#include <cassert>
#include <cmath>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <vector>

#ifdef __linux__
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "sys/sysinfo.h"
#include "sys/types.h"
#endif
/**
 * Reimplementation of the common getch function in in the conio.h header file for
 * linux. Pauses execution and waits for the user to press a key.
 * Kudos goes to:
 * http://cboard.cprogramming.com/faq-board/27714-faq-there-getch-conio-equivalent-linux-unix.html
 * @return  The char code of the pressed key
 */
inline int getch(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
    return ch;
}

inline std::string getCurrentTime()
{
    time_t rawtime;
    struct tm* timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    return std::string(asctime(timeinfo));
}

inline std::string print_numpy(double const* const* const mat, unsigned int n)
{
    std::stringstream ss;
    ss << "[";
    for (unsigned int i = 0; i < n; ++i) {
        ss << "[";
        for (unsigned int j = 0; j < n; ++j) {
            if (j == n - 1) {
                ss << mat[i][j];
            } else {
                ss << mat[i][j] << ",";
            }
        }
        if (i == n - 1) {
            ss << "]";
        } else {
            ss << "],";
        }
    }
    ss << "]" << std::endl;

    return ss.str();
}

inline bool icompare_pred(unsigned char a, unsigned char b)
{
    return std::tolower(a) == std::tolower(b);
}

inline bool eq_ignore_case(std::string const& x1, std::string const& x2)
{
    if (x1.length() == x2.length()) {
        return std::equal(x2.begin(), x2.end(), x1.begin(), icompare_pred);
    } else {
        return false;
    }
}

// Note: Last entry in rows is label
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
inline void readCSV(std::string const& path, std::vector<std::string> const& rows,
    std::vector<std::vector<float> >& X, std::vector<float>& Y)
{

    std::ifstream file(path);
    std::vector<std::string> header;

    if (!file.eof()) {
        std::string line;
        std::getline(file, line);
        std::stringstream ss(line);

        std::string entry;
        while (std::getline(ss, entry, ',')) {
            header.push_back(entry);
        }
    }

    assert(rows.size() != 0);

    std::vector<unsigned int> indices;
    unsigned int labelIndex;
    for (unsigned int i = 0; i < rows.size(); ++i) {
        unsigned int matchIndex = header.size();
        for (unsigned int j = 0; j < header.size(); ++j) {

            if (eq_ignore_case(header[j], rows[i])) {
                matchIndex = j;
                break;
            }
        }

        assert(matchIndex < header.size());
        if (i == rows.size() - 1) {
            labelIndex = matchIndex;
        } else {
            indices.push_back(matchIndex);
        }
    }

    while (!file.eof()) {
        std::string line;

        while (getline(file, line)) {
            std::stringstream ss(line);
            std::string entry;

            std::vector<float> x;
            unsigned int i = 0;

            while (getline(ss, entry, ',')) {
                for (auto j : indices) {
                    if (j == i) {
                        x.push_back(atof(entry.c_str()));
                        break;
                    }
                }

                if (i == labelIndex) {
                    float target = atof(entry.c_str());
                    if (target < 0) {
                        target = 0;
                    }
                    Y.push_back(target);
                }
                ++i;
            }

            X.push_back(x);
        }
    }
    file.close();
}
#pragma GCC diagnostic pop

template <typename T>
inline void minMaxData(
    std::vector<std::vector<T> > const& X, std::vector<float>& min, std::vector<float>& max)
{
    bool first = true;

    for (auto x : X) {
        for (unsigned int i = 0; i < x.size(); ++i) {
            if (first) {
                min.push_back(x[i]);
                max.push_back(x[i]);
            } else {
                if (x[i] < min[i]) {
                    min[i] = x[i];
                }
                if (x[i] > max[i]) {
                    max[i] = x[i];
                }
            }
        }
        first = false;
    }
}

template <typename T>
inline void normalizeData(std::vector<T>& X, float min, float max, float mean)
{
    for (unsigned int i = 0; i < X.size(); ++i) {
        if (max == min) {
            std::cout << "MIN/MAX is equal!" << std::endl;
        } else {
            X[i] = (X[i] - mean) / (max - min);
        }
    }
}

template <typename T>
inline void normalizeData(std::vector<std::vector<T> >& X, std::vector<float> const& min,
    std::vector<float> const& max)
{

    for (unsigned int i = 0; i < X.size(); ++i) {
        for (unsigned int j = 0; j < X[i].size(); ++j) {
            if (max[j] == min[j]) {
                //std::cout << "MIN/MAX for feature " << j << " is equal!" << std::endl;
            } else {
                X[i][j] = (X[i][j] - min[j]) / (max[j] - min[j]);
            }
        }
    }
}

inline void splitData(unsigned int numExamples, std::vector<unsigned int>& train, std::vector<unsigned int>& test,
    float ratio)
{
    std::vector<unsigned int> indices;
    for (unsigned int i = 0; i < numExamples; ++i) {
        indices.push_back(i);
    }

    std::random_shuffle(indices.begin(), indices.end());

    unsigned int i = 0;
    for (; i < numExamples * ratio; ++i) {
        test.push_back(indices[i]);
    }

    for (; i < numExamples; ++i) {
        train.push_back(indices[i]);
    }
}

inline void xval(unsigned int K, unsigned int numExamples, std::vector<std::vector<unsigned int> >& train,
    std::vector<std::vector<unsigned int> >& test)
{
    unsigned int bucketSize = numExamples / K;
    std::vector<unsigned int> indices;
    for (unsigned int i = 0; i < numExamples; ++i) {
        indices.push_back(i);
    }

    std::random_shuffle(indices.begin(), indices.end());

    for (unsigned int i = 0; i < K; ++i) {
        std::vector<unsigned int> ttrain;
        std::vector<unsigned int> ttest;

        unsigned int j = 0;
        for (; j < i * bucketSize; ++j) {
            ttrain.push_back(indices[j]);
        }

        for (; j < i * bucketSize + bucketSize; ++j) {
            ttest.push_back(indices[j]);
        }

        for (; j < indices.size(); ++j) {
            ttrain.push_back(indices[j]);
        }
        train.push_back(ttrain);
        test.push_back(ttest);
    }
}

#ifdef __linux__
int parseLine(char* line)
{
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p < '0' || *p > '9')
        p++;
    line[i - 3] = '\0';
    i = atoi(p);
    return i;
}

inline unsigned int readLinuxSystemfile(std::string const& path, std::string const& key)
{
    std::ifstream file(path);
    while (!file.eof()) {
        std::string line;

        while (getline(file, line)) {
            if (line.find(key) != std::string::npos) {
                std::regex rgx("[-+]?\\d+[\\.]?\\d*[eE]?[-+]?\\d*");
                std::smatch match;

                if (std::regex_search(line, match, rgx)) {
                    file.close();
                    return std::stoi(match[0]);
                }
            }
        }
    }
    return 0;
}

//Note: this value is in KB!
inline unsigned int getPhysicalMemoryUsed()
{
    return readLinuxSystemfile("/proc/self/status", "VmRSS:");
}

inline int getMaxPhysicalMemory()
{
    return readLinuxSystemfile("/proc/meminfo", "MemTotal:");
}

#endif

inline void refreshed_out(std::string const& str)
{
    static unsigned int lastLen = 0;
    std::cout << "\r";
    for (unsigned int i = 0; i < lastLen; ++i) {
        std::cout << " ";
    }

    std::cout << "\r" << str << std::flush;
    lastLen = str.length();
}

inline void cholesky(float const* const* const mat, float** L, unsigned int n)
{
    for (unsigned int i = 0; i < n; i++) {
        for (unsigned int j = 0; j < (i + 1); j++) {
            float s = 0;
            for (unsigned int k = 0; k < j; k++) {
                s += L[i][k] * L[j][k];
            }
            L[i][j] = (i == j) ? std::sqrt(mat[i][j] - s) : (1.0 / L[j][j] * (mat[i][j] - s));
        }
    }
}

inline float logdet(float const* const* const mat, unsigned int n)
{
    float** L = new float*[n];
    for (unsigned int i = 0; i < n; ++i) {
        L[i] = new float[n];
    }

    cholesky(mat, L, n);
    float det = 0;
    for (unsigned int i = 0; i < n; ++i) {
        det += log(L[i][i]);
    }

    for (unsigned int i = 0; i < n; ++i) {
        delete[] L[i];
    }
    delete[] L;
    return det;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
inline void invert(float const* const* const mat, float** out, unsigned int n)
{
    float** L = new float*[n];
    for (unsigned int i = 0; i < n; ++i) {
        L[i] = new float[n];
    }

    cholesky(mat, L, n);

    for (int j = n - 1; j >= 0; --j) {
        for (int i = j; i >= 0; --i) {
            double sum = 0.0;
            for (int k = i + 1; k < n; ++k) {
                sum += L[k][i] * out[k][j];
            }

            if (i == j) {
                out[i][j] = (1.0 / L[i][i] - sum) / L[i][i];
            } else {
                out[i][j] = -sum / L[i][i];
            }

            out[j][i] = out[i][j];
        }
    }

    for (unsigned int i = 0; i < n; ++i) {
        delete[] L[i];
    }
    delete[] L;
}
#pragma GCC diagnostic pop

#endif
