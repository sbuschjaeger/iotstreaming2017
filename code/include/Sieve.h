#ifndef GP_SIEVE_H
#define GP_SIEVE_H

#include "Helper.h"
#include "gpexpertworker.h"
#include <algorithm>
#include <future>
#include <limits>
#include <thread>
#include <tuple>

#include "kernel.h"

template <typename flt_t>
class Sieve {
private:
    flt_t **L;
    flt_t **sigma;
    flt_t **S;
    unsigned int n;
    flt_t curFunctionValue;

    Kernel<flt_t>* kernel;
    unsigned int const summarySize;
    unsigned int const dim;
    flt_t const sigmaNoise;
    flt_t const threshold;

    bool closed;

public:
    Sieve(Kernel<flt_t>* pKernel, unsigned int pSummarySize, unsigned int pDim, flt_t pSigmaNoise, flt_t pThreshold) :
            kernel(pKernel), summarySize(pSummarySize+1), dim(pDim), sigmaNoise(pSigmaNoise), threshold(pThreshold) {

        // TODO: Only save half here
        L = new flt_t*[summarySize];
        sigma = new flt_t*[summarySize];
        S = new flt_t*[summarySize];

        L[0] = new flt_t[summarySize];
        sigma[0] = new flt_t[summarySize];
        S[0] = new flt_t[dim];

        n = 0;
        curFunctionValue = 0;

        closed = false;
     }

    ~Sieve() {
        // Note: For n = 0, we already created L[0], sigma[0] and S[0]
        for (unsigned int i = 0; i < n + 1; ++i) {
            delete[] L[i];
            delete[] sigma[i];
            delete[] S[i];
        }

        delete[] L;
        delete[] sigma;
        delete[] S;
    }

    void clear() {
        n = 0;
        curFunctionValue = 0;
        closed = false;
    }

    unsigned int getN() const {
        return n;
    }

    flt_t getMaxDiagonal() const {
        return 1+sigmaNoise*sigmaNoise*kernel->max();
    }

    flt_t getMaxDelta() const {
        for (unsigned int j = 0; j <= n; j++) {
            double s = 0;
            for (unsigned int k = 0; k < j; k++) {
                s += L[n][k] * L[j][k];
            }

            // The best observation would produce a kernel function where all kernel entries are close to 0 exepct for the
            // diagonal element
            L[n][j] = (n == j) ? std::sqrt(getMaxDiagonal() - s) : (1.0 / L[j][j] * (0.0f - s));
        }

        return 2*log(L[n][n]);
    }

    flt_t getFunctionDelta(flt_t const * const elem) {
        // Update kernel matrix
        for (unsigned int i = 0; i < n; ++i) {
            sigma[i][n] = kernel->val(&S[i][0], elem);
            sigma[n][i] = sigma[i][n];
        }
        sigma[n][n] = getMaxDiagonal();

        // Update cholesky
        for (unsigned int j = 0; j <= n; j++) {
            double s = 0;
            for (unsigned int k = 0; k < j; k++) {
                s += L[n][k] * L[j][k];
            }
            L[n][j] = (n == j) ? std::sqrt(sigma[n][j] - s) : (1.0 / L[j][j] * (sigma[n][j] - s));
        }

        return 2*log(L[n][n]);
    }

    void updateSummary(flt_t const * const elem) {
        // Update summary
        for (unsigned int i = 0; i < dim; ++i) {
            S[n][i] = elem[i];
        }

        curFunctionValue += 2*log(L[n][n]);

        // Increase summary
        if (n < summarySize - 1) {
            n++;
            L[n] = new flt_t[summarySize];
            sigma[n] = new flt_t[summarySize];
            S[n] = new flt_t[dim];

            if (getMaxDelta() < (threshold/2 - curFunctionValue)/(summarySize - n)) {
                std :: cout << "Function Value cannot be reach anymore!" << std :: endl;
                std :: cout << "threshold = " << (threshold/2 - curFunctionValue)/(summarySize - n) << std :: endl;
                std :: cout << "maxThreshold = " << getMaxDelta() << std :: endl;
                std :: cout << "n = " << n << std :: endl;
                //closed = true;
            }
        }
    }

    bool next(flt_t const * const elem) {
        if (n < summarySize - 1 && !closed) {
            flt_t fdelta = getFunctionDelta(elem);

            if (n == 0  || fdelta >= (threshold/2 - curFunctionValue)/(summarySize - n)) {
            //if (n == 0  || fdelta >= (threshold - curFunctionValue)/(summarySize - n)) {
                updateSummary(elem);
//                std :: cout << "Adding with threshold = " << threshold << std :: endl;
//                std :: cout << " and curFunctionValue = " << curFunctionValue << std :: endl;
//                std :: cout << " and summarySize - n = " << summarySize - n << std :: endl;
//                std :: cout << " and delta = " << fdelta << std :: endl;
                if (n == summarySize - 1) {
                    closed = true;
                }
                return true;
            }
        }

        return false;
    }

    std::vector<std::vector<flt_t>> getSummary() const {
        std::vector<std::vector<flt_t>> summary;

        for (unsigned int i = 0; i < n; ++i) {
            std::vector<flt_t> temp;
            for (unsigned int j = 0; j < dim; ++j) {
                temp.push_back(S[i][j]);
            }
            summary.push_back(temp);
        }

        return summary;
    }

    std::vector<std::vector<flt_t>> getSigma() const {
        std::vector<std::vector<flt_t>> rSigma;

        for (unsigned int i = 0; i < n; ++i) {
            std::vector<flt_t> temp;
            for (unsigned int j = 0; j < n; ++j) {
                temp.push_back(sigma[i][j]);
            }
            rSigma.push_back(temp);
        }

        return rSigma;
    }

    bool isClosed() const {
        return closed;
    }

    bool isFull() const {
        return summarySize - 1 == n;
    }

    flt_t getCurFunctionValue() const {
        return curFunctionValue;
    }

    flt_t getThreshold() const {
        return threshold;
    }
};

#endif
